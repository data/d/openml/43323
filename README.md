# OpenML dataset: CorporateCreditRating

https://www.openml.org/d/43323

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

ContextAcorporatecreditratingexpressestheabilityofafirmtorepayitsdebttocreditorsCreditratingagenciesaretheentitiesresponsibletomaketheassessmentandgiveaverdictWhenabigcorporationfromtheUSoranywhereintheworldwantstoissueanewbondithiresacreditagencytomakeanassessmentsothatinvestorscanknowhowtrustworthyisthecompanyTheassessmentisbasedespeciallyinthefinancialsindicatorsthatcomefromthebalancesheetSomeofthemostimportantagenciesintheworldareMoodysFitchandStandardandPoorsContentAlistof2029creditratingsissuedbymajoragenciessuchasStandardandPoorstobigUSfirmstradedonNYSEorNasdaqfrom2010to2016Thereare30featuresforeverycompanyofwhich25arefinancialindicatorsTheycanbedividedinLiquidityMeasurementRatioscurrentRatioquickRatiocashRatiodaysOfSalesOutstandingProfitabilityIndicatorRatiosgrossProfitMarginoperatingProfitMarginpretaxProfitMarginnetProfitMargineffectiveTaxRatereturnOnAssetsreturnOnEquityreturnOnCapitalEmployedDebtRatiosdebtRatiodebtEquityRatioOperatingPerformanceRatiosassetTurnoverCashFlowIndicatorRatiosoperatingCashFlowPerSharefreeCashFlowPerSharecashPerShareoperatingCashFlowSalesRatiofreeCashFlowOperatingCashFlowRatioFormoreinformationaboutfinancialindicatorsvisithttpsfinancialmodelingprepcommarketindexesmajormarketsTheadditionalfeaturesareNameSymbolfortradingRatingAgencyNameDateandSectorThedatasetisunbalancedhereisthefrequencyofratingsAAA7AA89A398BBB671BB490B302CCC64CC5C2D1AcknowledgementsThisdatasetwaspossiblethankstofinancialmodelingprepandopendatasoftthesourcesofthedataToseehowthedatawasintegratedandreshapedcheckhereInspirationIsitpossibletoforecasttheratinganagencywillgivetoacompanybasedonitsfinancials

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43323) of an [OpenML dataset](https://www.openml.org/d/43323). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43323/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43323/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43323/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

